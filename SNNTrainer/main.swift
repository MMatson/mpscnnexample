//
//  main.swift
//  SNNTrainer
//
//  Created by Mark Matson on 6/27/20.
//  Copyright © 2020 Mark Matson. All rights reserved.
//

import Foundation
import Metal
import MetalPerformanceShaders


let DEVICE = MTLCreateSystemDefaultDevice()!
let COMMAND_QUEUE = DEVICE.makeCommandQueue()!
let MNIST = MNISTDataManager()

let trainer = SNNTrainer()

trainer.trainNetwork()
