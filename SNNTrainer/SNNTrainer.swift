//
//  SNNTrainer.swift
//  ScrawlNN Trainer
//
//  Created by Mark Matson on 6/27/20.
//

import Foundation
import MetalPerformanceShaders

let NUM_EPOCHS = 1
let NUM_ITERATIONS_PER_EPOCH = MNIST_NUM_TRAIN_IMAGES / BATCH_SIZE
let BATCH_SIZE = 50

var NUM_CORRECT: Int = 0
var NUM_IN_TESTSET: Int = 60000


let networkDirectory = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("models").appendingPathComponent("networks")


class SNNTrainer {
    let lossDescriptor: MPSCNNLossDescriptor
    var trainingGraph: MPSNNGraph? = nil
    
    init() {
        self.lossDescriptor = MPSCNNLossDescriptor(type: .softMaxCrossEntropy, reductionType: .sum)
    }

    
    // MARK: Networking Training
    
    func trainNetwork() {
        trainingGraph = MPSNNGraph(device: DEVICE, resultImage: makeTrainingGraph(), resultImageIsNeeded: true)
        trainingGraph?.options = .none
        trainingGraph?.format = .float32
        
        print(trainingGraph.debugDescription)
        
        var latestCommandBuffer: MPSCommandBuffer? = nil
        for i in 0..<NUM_EPOCHS {
            for j in 0..<NUM_ITERATIONS_PER_EPOCH {
                latestCommandBuffer = trainingIteration(i * NUM_ITERATIONS_PER_EPOCH + j)
            }
        }
        latestCommandBuffer!.waitUntilCompleted()
    }
    
    func trainingIteration(_ iter: Int) -> MPSCommandBuffer {
        let doubleBufferSemaphore = DispatchSemaphore(value: 2)
        let _ = doubleBufferSemaphore.wait(timeout: .distantFuture)
        let commandBuffer = MPSCommandBuffer(from: COMMAND_QUEUE)
        
        let (imageBatch, labelBatch) = MNIST.getRandomTrainingBatch(with: DEVICE, batchSize: BATCH_SIZE)

        print("Training Iteration \(iter) started.")
        let returnBatch = trainingGraph!.encodeBatch(to: commandBuffer, sourceImages: [imageBatch], sourceStates: [labelBatch])
        commandBuffer.addCompletedHandler { commandBuffer in
            doubleBufferSemaphore.signal()
            print("Training Iteration \(iter) finished.")
            print(returnBatch as Any)
            if let err = commandBuffer.error {
                print("CommandBuffer Error: \(err)")
            }
        }
        commandBuffer.commit()
        return commandBuffer
    }
    
    
    // MARK: Training Graph
    
    func makeTrainingGraph() -> MPSNNImageNode {
        let conv1 = MPSCNNConvolutionNode(source: MPSNNImageNode(handle: nil), weights: NetworkSource("conv1", 5, 5, 1, 32))
        let pool1 = MPSCNNPoolingMaxNode(source: conv1.resultImage, filterSize: 2)
        let conv2 = MPSCNNConvolutionNode(source: pool1.resultImage, weights: NetworkSource("conv2", 5, 5, 32, 64))
        let pool2 = MPSCNNPoolingMaxNode(source: conv2.resultImage, filterSize: 2)
        let fc1 = MPSCNNFullyConnectedNode(source: pool2.resultImage, weights: NetworkSource("fc1", 7, 7, 64, 1024))
        let fc2 = MPSCNNFullyConnectedNode(source: fc1.resultImage, weights: NetworkSource("fc2", 1, 1, 1024, 10))
        lossDescriptor.weight = 1.0 / Float(BATCH_SIZE)
        let loss = MPSCNNLossNode(source: fc2.resultImage, lossDescriptor: lossDescriptor)
        let fc2G = fc2.gradientFilter(withSource: loss.resultImage)
        let fc1G = fc1.gradientFilter(withSource: fc2G.resultImage)
        let pool2G = pool2.gradientFilter(withSource: fc1G.resultImage)
        let conv2G = conv2.gradientFilter(withSource: pool2G.resultImage)
        let pool1G = pool1.gradientFilter(withSource: conv2G.resultImage)
        let conv1G = conv1.gradientFilter(withSource: pool1G.resultImage)
        return conv1G.resultImage
//        let grad = loss.trainingGraph(withSourceGradient: nil, nodeHandler: nil)!
//        print(grad[0].resultImage)
//        return grad[0].resultImage
    }
    
    
    // MARK: Inference Graph
    
    func makeInferenceGraph() -> MPSNNImageNode {
        //When inferring PKDrawing from MNIST-Trained network, scale to 28x28px
        let scale = MPSNNLanczosScaleNode(source: MPSNNImageNode(handle: nil), outputSize: MTLSize(width: 28, height: 28, depth: 3))
        let conv1 = MPSCNNConvolutionNode(source: scale.resultImage, weights: NetworkSource("conv1", 5, 5, 1, 32))
        let pool1 = MPSCNNPoolingMaxNode(source: conv1.resultImage, filterSize: 2)
        let conv2 = MPSCNNConvolutionNode(source: pool1.resultImage, weights: NetworkSource("conv2", 5, 5, 32, 64))
        let pool2 = MPSCNNPoolingMaxNode(source: conv2.resultImage, filterSize: 2)
        let fc1 = MPSCNNFullyConnectedNode(source: pool2.resultImage, weights: NetworkSource("fc1", 7, 7, 64, 1024))
        let fc2 = MPSCNNFullyConnectedNode(source: fc1.resultImage, weights: NetworkSource("fc2", 1, 1, 1024, 10))
        return fc2.resultImage
    }
    
    
    // MARK: CNN Data Source
    
    class NetworkSource: NSObject, MPSCNNConvolutionDataSource {
        let name: String
        let kernelWidth: Int
        let kernelHeight: Int
        let inputFeatureChannels: Int
        let outputFeatureChannels: Int
        let optimizer: MPSNNOptimizerAdam
        let cnnDescriptor: MPSCNNConvolutionDescriptor
        let cnnState: MPSCNNConvolutionWeightsAndBiasesState
        let weightsVector, biasesVector: MPSVector
        let weightsMomentumVector, biasesMomentumVector: MPSVector
        let weightsVelocityVector, biasesVelocityVector: MPSVector
        let time: Float = 0
        
        var data: Data
        
        init(_ name: String, _ kernelWidth: Int, _ kernelHeight: Int, _ inputFeatureChannels: Int, _ outputFeatureChannels: Int) {
            self.name = name
            self.kernelWidth = kernelWidth
            self.kernelHeight = kernelHeight
            self.inputFeatureChannels = inputFeatureChannels
            self.outputFeatureChannels = outputFeatureChannels
            self.cnnDescriptor = MPSCNNConvolutionDescriptor(kernelWidth: kernelWidth, kernelHeight: kernelHeight, inputFeatureChannels: inputFeatureChannels, outputFeatureChannels: outputFeatureChannels)
            

            self.cnnState = MPSCNNConvolutionWeightsAndBiasesState(device: DEVICE, cnnConvolutionDescriptor: self.cnnDescriptor)
            
            let optimizerDescriptor = MPSNNOptimizerDescriptor(learningRate: 0.001, gradientRescale: 1.0, regularizationType: .None, regularizationScale: 1.0)
            self.optimizer = MPSNNOptimizerAdam(device: DEVICE, beta1: 0.9, beta2: 0.999, epsilon: 0.00000001, timeStep: 0, optimizerDescriptor: optimizerDescriptor)
//            self.optimizer = MPSNNOptimizerStochasticGradientDescent(device: DEVICE, learningRate: 0.001)
            
            
            let weightsLength = inputFeatureChannels * kernelWidth * kernelHeight * outputFeatureChannels
            let weightsVectorDescriptor = MPSVectorDescriptor(length: weightsLength, dataType: .float32)
            self.weightsVector = MPSVector(device: DEVICE, descriptor: weightsVectorDescriptor)
            self.weightsMomentumVector = MPSVector(device: DEVICE, descriptor: weightsVectorDescriptor)
            self.weightsVelocityVector = MPSVector(device: DEVICE, descriptor: weightsVectorDescriptor)
            
            let biasesVectorDescriptor = MPSVectorDescriptor(length: outputFeatureChannels, dataType: .float32)
            self.biasesVector = MPSVector(device: DEVICE, descriptor: biasesVectorDescriptor)
            self.biasesMomentumVector = MPSVector(device: DEVICE, descriptor: biasesVectorDescriptor)
            self.biasesVelocityVector = MPSVector(device: DEVICE, descriptor: biasesVectorDescriptor)
            
            
            let distributionDiscriptor = MPSMatrixRandomDistributionDescriptor.uniformDistributionDescriptor(withMinimum: -0.2, maximum: 0.2)
            let randomKernel = MPSMatrixRandomMTGP32(device: DEVICE, destinationDataType: .float32, seed: 1, distributionDescriptor: distributionDiscriptor)

            let commandBuffer = MPSCommandBuffer(from: COMMAND_QUEUE)
            randomKernel.encode(commandBuffer: commandBuffer, destinationVector: weightsVector)
            commandBuffer.commit()
            commandBuffer.waitUntilCompleted()
            
            self.data = Data(bytes: weightsVector.data.contents(), count: weightsVector.vectorBytes)
            
        }
        
        
        func dataType() -> MPSDataType {
            return .float32
        }
        
        
        func biasTerms() -> UnsafeMutablePointer<Float>? {
            return UnsafeMutablePointer<Float>(self.biasesVector.data.contents().assumingMemoryBound(to: Float.self))
        }
        
        
        func label() -> String? {
            return name
        }
        
        
        func copy(with zone: NSZone? = nil) -> Any {
            fatalError("copyWithZone not implemented")
        }
        
        
        func load() -> Bool {
//            let url = networkDirectory.appendingPathComponent("\(name).bin")
//            do {
//                data = try Data(contentsOf: url)
//                return true
//            } catch {
//                print("error: courld not load \(url): \(error)")
//            }
//            return false
//            guard let commandBuffer = COMMAND_QUEUE.makeCommandBuffer() else { return false }
//            cnnState.synchronize(on: commandBuffer)
//            commandBuffer.commit()
//            commandBuffer.waitUntilCompleted()
            return true
        }
        
        
        func descriptor() -> MPSCNNConvolutionDescriptor {
            return self.cnnDescriptor
        }
        
        
        func weights() -> UnsafeMutableRawPointer {
            return UnsafeMutableRawPointer(weightsVector.data.contents())
//            return UnsafeMutableRawPointer(mutating: (data as NSData).bytes)
        }
        
        
        func purge() {
            //data = nil
        }
        
        
        func update(with commandBuffer: MTLCommandBuffer, gradientState: MPSCNNConvolutionGradientState, sourceState: MPSCNNConvolutionWeightsAndBiasesState) -> MPSCNNConvolutionWeightsAndBiasesState? {
            
            optimizer.encode(commandBuffer: commandBuffer, convolutionGradientState: gradientState, convolutionSourceState: sourceState, inputMomentumVectors: [weightsMomentumVector, biasesMomentumVector], inputVelocityVectors: [weightsVelocityVector, biasesVelocityVector], resultState: sourceState)
//            guard let commandBuffer = COMMAND_QUEUE.makeCommandBuffer() else { return nil }
//            self.cnnState.synchronize(on: commandBuffer)
//            commandBuffer.commit()
//            commandBuffer.waitUntilCompleted()
            return sourceState
        }
    }

}

