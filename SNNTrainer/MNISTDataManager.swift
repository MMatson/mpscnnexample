//
//  MNISTDataManager.swift
//  Numeral
//
//  Created by Mark Matson on 6/27/20.
//  Copyright © 2020 Mark Matson. All rights reserved.
//

import Foundation
import Metal
import MetalPerformanceShaders

let MNIST_IMAGES_BYTE_PREFIX = 16
let MNIST_LABELS_BYTE_PREFIX = 8
let MNIST_IMAGE_HEIGHT = 28
let MNIST_IMAGE_WIDTH = 28

let MNIST_NUM_TRAIN_IMAGES = 60000
let MNIST_NUM_TEST_IMAGES = 10000


let MNIST_DIRECTORY = "http://yann.lecun.com/exdb/mnist/"
let WORKING_DIRECTORY = defaultDirectory.appendingPathComponent("MNIST", isDirectory: true)


//let testSet = dataManager.fetchMNISTDataset(
//    localStorageDirectory: WORKING_DIRECTORY,
//    remoteBaseDirectory: MNIST_DIRECTORY,
//    imagesFilename: "t10k-images-idx3-ubyte",
//    labelsFilename: "t10k-labels-idx1-ubyte"
//)

struct LabeledImage {
    var image: [UInt8]
    var label: Int
}

class MNISTDataManager {
    private var trainingSet: [LabeledImage] = []
    
    init() {
        fetchMNISTDataset(
            localStorageDirectory: WORKING_DIRECTORY,
            remoteBaseDirectory: MNIST_DIRECTORY,
            imagesFilename: "train-images-idx3-ubyte",
            labelsFilename: "train-labels-idx1-ubyte"
        )
    }
    
    public func getRandomTrainingBatch(with device: MTLDevice, batchSize: Int) -> (randomImageBatch: [MPSImage], randomLabelBatch: [MPSState]) {
        var trainingImageBatch: [MPSImage] = []
        var trainingLabelBatch: [MPSState] = []

        for _ in 0..<batchSize {
            let trainingImageDescriptor = MPSImageDescriptor(channelFormat: .unorm8, width: 28, height: 28, featureChannels: 1, numberOfImages: 1, usage: [.shaderRead])
            let randomIndex = Int.random(in: 0...(MNIST_NUM_TRAIN_IMAGES - 1))
            
            let trainingImage = MPSImage(device: DEVICE, imageDescriptor: trainingImageDescriptor)
            let imageData = Data(trainingSet[randomIndex].image)
            imageData.withUnsafeBytes({ rawBufferPointer in
                trainingImage.writeBytes(rawBufferPointer, dataLayout: .HeightxWidthxFeatureChannels, imageIndex: 0)
            })
            
            var labels: [Float] = Array(repeating: 0.0, count: 12)
            let correctLabel = trainingSet[randomIndex].label
            labels[correctLabel] = 1.0
            let labelsData = Data(bytes: labels, count: 12 * MemoryLayout<Float>.stride)
            let labelDescriptor = MPSCNNLossDataDescriptor(data: labelsData, layout: .HeightxWidthxFeatureChannels, size: MTLSize(width: 1, height: 1, depth: 12))!
            let lossLabel = MPSCNNLossLabels(device: DEVICE, labelsDescriptor: labelDescriptor)
            
            trainingImageBatch.append(trainingImage)
            trainingLabelBatch.append(lossLabel)
        }

        
        return (trainingImageBatch, trainingLabelBatch)
    }
    
    public func fetchMNISTDataset(localStorageDirectory: URL, remoteBaseDirectory: String, imagesFilename: String, labelsFilename: String) {
        guard let remoteRoot = URL(string: remoteBaseDirectory) else {
            fatalError("Failed to create MNIST root url: \(remoteBaseDirectory)")
        }
        
        // FIXME: File extension and filename defined in different files.
        let imagesData = fetchResource(
            fileName: imagesFilename,
            fileExtension: "gz",
            remoteRoot: remoteRoot,
            localStorageDirectory: localStorageDirectory
        )
        let labelsData = fetchResource(
            fileName: labelsFilename,
            fileExtension: "gz",
            remoteRoot: remoteRoot,
            localStorageDirectory: localStorageDirectory
        )
        
        let images = [UInt8](imagesData).dropFirst(MNIST_IMAGES_BYTE_PREFIX)
        let labels = [UInt8](labelsData).dropFirst(MNIST_LABELS_BYTE_PREFIX).map({Int($0)})
        
        var labeledImages: [LabeledImage] = []
        
        let imageByteSize = MNIST_IMAGE_WIDTH * MNIST_IMAGE_HEIGHT
        for imageIndex in 0..<labels.count {
            let baseAddress = images.startIndex + imageIndex * imageByteSize
            let data = [UInt8](images[baseAddress..<(baseAddress + imageByteSize)])
            labeledImages.append(LabeledImage(image: data, label: labels[imageIndex]))
        }
        
        print("\(imagesFilename): \(labeledImages.count)")
//        print("\(labeledImages.first!.image)")
//        print("\(labeledImages.first!.label)")

        self.trainingSet = labeledImages
    }
    
}

let defaultDirectory = try! FileManager.default.url(
        for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        .appendingPathComponent("models").appendingPathComponent("datasets")
    
func fetchResource(
    fileName: String,
    fileExtension: String,
    remoteRoot: URL,
    localStorageDirectory: URL
) -> Data {
    let resource = Resource(
        fileName: fileName,
        fileExtension: fileExtension,
        remoteRoot: remoteRoot,
        localStorageDirectory: localStorageDirectory
    )
    let localURL = downloadResource(resource)
    do {
        let data = try Data(contentsOf: localURL)
        return data
    } catch {
        fatalError("Failed to extract contents of resource: \(localURL)")
    }
}


func downloadResource(_ resource: Resource) -> URL {
    print("Loading resource: \(resource.fileName)")
    let localURL = resource.localURL
    if !FileManager.default.fileExists(atPath: localURL.path) {
        print("File does not exist locally at: \(localURL.path) and must be fetched.")
        fetchFromRemoteAndSave(resource)
    }
    return localURL
}


func fetchFromRemoteAndSave(_ resource: Resource) {
    let remoteLocation = resource.remoteURL
    let archiveLocation = resource.localStorageDirectory
    do {
        print("Fetching URL: \(remoteLocation)")
        try download(from: remoteLocation, to: archiveLocation)
    } catch {
        fatalError("Failed to fetch and save resource with error: \(error)")
    }
    print("Archive saved to: \(archiveLocation.path)")
    extractArchive(at: resource.archiveURL, to: resource.localStorageDirectory, fileExtension: resource.fileExtension)
}


func download(from source: URL, to destination: URL) throws {
    let destinationFile: String
    
    if destination.hasDirectoryPath {
        try createDirectoryIfMissing(at: destination.path)
        let fileName = source.lastPathComponent
        destinationFile = destination.appendingPathComponent(fileName).path
    } else {
        try createDirectoryIfMissing(at: destination.deletingLastPathComponent().path)
        destinationFile = destination.path
    }
    
    let downloadedFile = try Data(contentsOf: source)
    try downloadedFile.write(to: URL(fileURLWithPath: destinationFile))
}


func createDirectoryIfMissing(at path: String) throws {
    guard !FileManager.default.fileExists(atPath: path) else { return }
    try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
}


func extractArchive(at archive: URL, to localStorageDirectory: URL, fileExtension: String? = nil) {
    let archivePath = archive.path
    
    #if os(macOS)
        let binaryLocation = "/usr/bin/"
    #else
        let binaryLocation = "/bin/"
    #endif
    
    let toolName: String
    let arguments: [String]
    let adjustedPathExtension: String
    
    if archive.path.hasSuffix(".tar.gz") {
        adjustedPathExtension = "tar.gz"
    } else {
        adjustedPathExtension = archive.pathExtension
    }
    
    switch fileExtension ?? adjustedPathExtension {
    case "gz":
        toolName = "gunzip"
        arguments = [archivePath]
    case "tar":
        toolName = "tar"
        arguments = ["xf", archivePath, "-C", localStorageDirectory.path]
    case "tar.gz":
        toolName = "tar"
        arguments = ["xzf", archivePath, "-C", localStorageDirectory.path]
    default:
        fatalError("No archiver listed for extension: \(fileExtension ?? adjustedPathExtension)")
    }
    
    let toolLocation = "\(binaryLocation)\(toolName)"
    
    let task = Process()
    task.executableURL = URL(fileURLWithPath: toolLocation)
    task.arguments = arguments
    do {
        try task.run()
        task.waitUntilExit()
    } catch {
        fatalError("Failed to extract \(archivePath) with error: \(error)")
    }
    
    if FileManager.default.fileExists(atPath: archivePath) {
        do {
            try FileManager.default.removeItem(atPath: archivePath)
        } catch {
            print("Cound not delete \(archivePath) with error: \(error)")
        }
    }
    
}


struct Resource {
    let fileName: String
    let fileExtension: String
    let remoteRoot: URL
    let localStorageDirectory: URL
    
    var localURL: URL {
        localStorageDirectory.appendingPathComponent(fileName)
    }
    
    var remoteURL: URL {
        remoteRoot.appendingPathComponent(fileName).appendingPathExtension(fileExtension)
    }

    var archiveURL: URL {
        localURL.appendingPathExtension(fileExtension)
    }
}
